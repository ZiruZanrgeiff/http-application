package com.ronildo.httpaplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class MainActivity extends AppCompatActivity {

    private Gson gson = new Gson();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String URL = "https://reqres.in/api/users";

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray arr = response.getJSONArray("data");
                            for (int i = 0; i < arr.length(); i++) {
                                JSONObject object = arr.getJSONObject(i);
                                User user = gson.fromJson(arr.getJSONObject(i).toString(), User.class);
                                Log.i("Nome completo: ", user.getFullName());

                                Iterator keys = object.keys();
                                while(keys.hasNext()) {
                                    // loop to get the dynamic key
                                    String currentDynamicKey = (String)keys.next();
                                    // get the value of the dynamic key
                                    Object currentDynamicValue = object.get(currentDynamicKey);
                                    Log.i(user.getFullName() + " : ", currentDynamicKey  + " :"+ currentDynamicValue.toString());
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Request Fail: ", error.toString());

                    }
                }


        );

        requestQueue.add(jsonObjectRequest);
    }
}
